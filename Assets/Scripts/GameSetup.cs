﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GameSetup : NetworkBehaviour {

	public Sprite[] playerSprites = new Sprite[4];

	public GameObject[] playerPrefabs = new GameObject[4];

	public GameObject[] playerStartPositions = new GameObject[4];

	public GameObject spinner;

	public Image currentPlayerSprite;

	private PlayerSelection.PlayerInfo[] playerInfoList;
	private int connID;

	public Image cardIcon;
	public Sprite[] cards = new Sprite[6];

	// Use this for initialization
	void Start () {
		
		//cardIcon.enabled = false;

		//connID = GameObject.Find("PlayerSelection").GetComponent<PlayerSelection>().uniqueConnectionID;

		if (!NetworkServer.active) {
			sendMessageToServerToCreatePlayer (ClientSettings.characterSelection);
		}
		else {
			addPlayerPrefab (ClientSettings.characterSelection, NetworkServer.localConnections [0]);
		}

		if (NetworkServer.active) {
			this.gameObject.AddComponent<GameTurn> ();
		}
	}

	private void sendMessageToServerToCreatePlayer(int playerSelection) {
		MSGTYPE.CreatePlayerMessage cm = new MSGTYPE.CreatePlayerMessage();
		cm.player = playerSelection;
		NetworkClient.allClients[0].Send (MSGTYPE.CREATEPLAYER, cm);
	}
		

	public void addPlayerPrefab(int playerID, NetworkConnection conn) {
		GameObject playerPrefab = playerPrefabs[0];
		Vector3 startPos = new Vector3();
		playerPrefab=playerPrefabs[playerID];
		startPos = playerStartPositions[playerID].transform.position;
			

		GameObject player = (GameObject)Instantiate(playerPrefab, startPos, 
		                        Quaternion.Euler(new Vector3(0,0,0)));


		Debug.Log (ClientScene.ready);
		NetworkServer.AddPlayerForConnection(conn, player, 0);

		NetworkServer.Spawn (player);
	}


	private void changeImageSprite(int playerID) {
		currentPlayerSprite.sprite = playerSprites[playerID];
	}

	public void showCardSelection(int choice) {
		cardIcon.sprite = cards[choice-1];
		cardIcon.enabled = true;
		Invoke ("hideCardSelection", 3.0f);
	}

	private void hideCardSelection(){
		cardIcon.enabled = false;
	}

}
