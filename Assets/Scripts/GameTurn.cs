﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class GameTurn : MonoBehaviour
{

	//private PlayerSelection.PlayerInfo[] playerInfoList;
	ReadOnlyCollection<NetworkConnection> connections;

	public int whichPlayer = 0;

	void Start ()
	{
		
		//playerInfoList = new PlayerSelection.PlayerInfo[3];
		//GameObject.Find("PlayerSelection").GetComponent<PlayerSelection>().SyncList_playerInfo.CopyTo(playerInfoList,0);
		connections = NetworkServer.connections;

		for (int i = 0; i < connections.Count; i++) {
			Debug.Log (connections[i].connectionId);
		}
		MSGTYPE.PlayerTurnMessage pt = new MSGTYPE.PlayerTurnMessage ();
		NetworkServer.SendToClient (connections[0].connectionId, MSGTYPE.PLAYERTURN, pt);
	}

	public int getNetworkConnectionIDofCurrentPlayer ()
	{
		return connections[whichPlayer].connectionId;
	}

	public int getNetworkConnectionIDofNextPlayer ()
	{
		whichPlayer++;
		if (whichPlayer >=connections.Count)
			whichPlayer = 0;
		return connections[whichPlayer].connectionId;
	}

	public int getNetworkConnectionIDofPreviousPlayer ()
	{
		int prevPlayer = whichPlayer - 1;
		if (prevPlayer < 0)
			prevPlayer = connections.Count - 1;
		return connections[whichPlayer].connectionId;
	}


	public void sendNextTurnMessage ()
	{

			MSGTYPE.PlayerTurnMessage pt = new MSGTYPE.PlayerTurnMessage ();
		int id = getNetworkConnectionIDofNextPlayer ();

			NetworkServer.SendToClient (id, MSGTYPE.PLAYERTURN, pt);
		}

}
