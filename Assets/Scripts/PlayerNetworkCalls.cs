﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerNetworkCalls : NetworkBehaviour {

	[Command]
	public void Cmd_EndGame (){
		GameObject.Find ("NetworkManager").GetComponent<MyNetworkManager> ().ServerChangeScene ("Win Scene");
	}

	[Command]
	public void Cmd_EndTurn (){
		GameObject.Find ("GameCode").GetComponent<GameTurn> ().sendNextTurnMessage ();
	}
}
