﻿using UnityEngine.Networking;

public class MSGTYPE {

	public const int PLAYERTURN = 1001;
	public const int PLAYERINITIALSELECTION = 1004;
	public const int PROVIDEUNIQUEID = 1006;
	public const int PLAYERMOVES = 1007;
	public const int CARDCHOICE = 1008;
	public const int GAMEOVER = 1009;

	public const int CREATEPLAYER = 1010;

	public class PlayerTurnMessage : MessageBase {
	
	}

	public class PlayerInitialSelectionMessage : MessageBase {
		public int whichOne;
	}
		
	public class ProvideUniqueIDMessage : MessageBase {
		public int connID;
	}

	public class PlayerMovesMessage : MessageBase {
		public int moves;
	}

	public class CardChoiceMessage : MessageBase {
		public int choice;
	}

	public class GameOverMessage : MessageBase {
		public int whoWon;
	}

	public class CreatePlayerMessage : MessageBase {
		public int player;
	}
}
