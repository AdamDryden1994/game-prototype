﻿public class Cards {

	public const int MISS_A_TURN = 1;
	public const int EXTRA_TURN = 2;
	public const int MOVE_FORWARD_2 = 3;
	public const int MOVE_BACK_2 = 4;
	public const int NEXT_PLAYER_MISS_A_TURN = 5;
	public const int PREVIOUS_PLAYER_MISS_A_TURN = 6;

}
