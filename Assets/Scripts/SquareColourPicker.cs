﻿using UnityEngine;
using System.Collections;

public class SquareColourPicker : MonoBehaviour
{

	//public static int counter = 0;

	public Material redmaterial;
	public Material bluematerial;
	public Material greenmaterial;
	public Material yellowmaterial;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	/*void OnMouseDown () {
		counter++;
		switch (counter % 4) {
		case 0:
			this.transform.GetComponentInChildren<Renderer> ().material = redmaterial;
			break;
		case 1:
			this.transform.GetComponentInChildren<Renderer> ().material = bluematerial;
			break;
		case 2:
			this.transform.GetComponentInChildren<Renderer> ().material = yellowmaterial;
			break;
		case 3:
			this.transform.GetComponentInChildren<Renderer> ().material = greenmaterial;
			break;
		}

	}*/

	public void switchColour (int player)
	{
		switch (player) {
		case 1:
			this.transform.GetComponentInChildren<Renderer> ().material = redmaterial;
			break;
		case 2:
			this.transform.GetComponentInChildren<Renderer> ().material = bluematerial;
			break;
		case 3:
			this.transform.GetComponentInChildren<Renderer> ().material = yellowmaterial;
			break;
		case 4:
			this.transform.GetComponentInChildren<Renderer> ().material = greenmaterial;
			break;
		}
	}

	void OnMouseOver ()
	{
		Color color = this.transform.GetComponentInChildren<Renderer> ().material.color;
		color.a = 0.25f;
		this.transform.GetComponentInChildren<Renderer> ().material.color = color;
	}

	void OnMouseExit ()
	{
		Color color = this.transform.GetComponentInChildren<Renderer> ().material.color;
		color.a = 1.0f;
		this.transform.GetComponentInChildren<Renderer> ().material.color = color;
	}
}
