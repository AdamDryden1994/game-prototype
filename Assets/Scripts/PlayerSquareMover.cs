﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerSquareMover : MonoBehaviour
{
	public int playerID = 1;
	public bool isMoving = false;
	private float fracMove;
	private Vector3 startPosition;
	private Vector3 endPosition;
	private GameObject square;

	private int counter = 0;

	public bool myTurn = false;
	// Use this for initialization
	void Start ()
	{
		fracMove = 0;
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButtonDown (0) && !isMoving && counter < 4 && myTurn) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.transform.tag == "boardsquare") {
					
					startPosition = this.transform.position;
					endPosition = hit.transform.position;

					if (Vector3.Distance (startPosition, endPosition) < 22) {
						isMoving = true;
						square = hit.transform.gameObject;
						counter++;
					}

					this.transform.LookAt (hit.transform);
				}
			}
		}

		if (fracMove >= 1f && isMoving) { //end turn
			isMoving = false;
			square.GetComponent<SquareColourPicker> ().switchColour (playerID);
			if (checkFinished ()) {
				this.GetComponent<PlayerNetworkCalls>().Cmd_EndGame ();
			}
			if(checkBomb()) {
				counter = 4;
			}
			if(counter == 4){
					myTurn = false;
				this.GetComponent<PlayerNetworkCalls>().Cmd_EndTurn ();	
			}
			fracMove = 0;
		}

		if (isMoving) {
			fracMove += 0.015f;
			transform.position = Vector3.Lerp (startPosition, endPosition, fracMove);
		}

		if (isMoving) {
			GetComponent<Animator> ().SetBool ("WALKING", true);
		} else {
			GetComponent<Animator> ().SetBool ("WALKING", false);
		}

			


		/*if (isMoving) {
			GetComponent<Animator> ().SetBool ("WALKING", true);
		} else {
			GetComponent<Animator> ().SetBool ("WALKING", false);
		}*/

	}


	public void movePlayer () {
		myTurn = true;
		counter = 0;
	}

	private bool checkFinished() {
		if (Vector3.Distance (this.transform.position, GameObject.Find ("Finishline").transform.position) < 20) {
		return true;
	    } else {
			return false;
		}
	}

	private bool checkBomb() {
		if (Vector3.Distance (this.transform.position, GameObject.Find ("Bomb").transform.position) < 30) {
			return true;
		} else {

		return false;
	}


}
}