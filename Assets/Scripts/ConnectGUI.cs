﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConnectGUI : MonoBehaviour
{
	public Button connectBtn;
	
	void Start() {
		GameObject nm = GameObject.Find("NetworkManager");
		connectBtn.onClick.AddListener(delegate () { 
			(nm.GetComponent<MyNetworkManager>()).buttonClicked(connectBtn.name); 
		});
	}	
}
