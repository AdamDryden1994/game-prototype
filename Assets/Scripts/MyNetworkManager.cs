﻿ using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MyNetworkManager : NetworkManager
{
	private int whoWon;

	public void buttonClicked (string btn)
	{
		switch (btn) {
		case "host_btn":
			this.StartHost ();
			break;
		case "join_btn":
			Application.LoadLevel ("CharacterSelecter");
			break;
		case "connect_btn":
			this.StartClient ();
			break;
		}
	}

	public override void OnStartServer ()
	{
		base.OnStartServer ();
		NetworkServer.RegisterHandler (MSGTYPE.PLAYERINITIALSELECTION, onPlayerInitialSelectionFromClientMessage);
		NetworkServer.RegisterHandler (MSGTYPE.CREATEPLAYER, onCreatePlayerFromClientMessage);
	}

	public override void OnStartClient (NetworkClient client){
		base.OnStartClient (client);
		client.RegisterHandler (MSGTYPE.PLAYERTURN, onPlayerTurnMessage);
		client.RegisterHandler (MSGTYPE.PROVIDEUNIQUEID, onProvideUniqueIDMessage);
		client.RegisterHandler (MSGTYPE.PLAYERMOVES, onPlayerMovesMessage);
		client.RegisterHandler (MSGTYPE.CARDCHOICE, onCardChoiceMessage);
		client.RegisterHandler (MSGTYPE.GAMEOVER, onGameOverMessage);
	}

	public override void OnClientDisconnect (NetworkConnection conn){
		base.OnClientDisconnect (conn);
	}
	
	public override void OnServerConnect (NetworkConnection conn){
		base.OnServerConnect (conn);
	}

	public override void OnClientSceneChanged (NetworkConnection conn)
	{
		base.OnClientSceneChanged (conn);
		if(!conn.isReady) ClientScene.Ready (conn);
		NetworkServer.SpawnObjects ();
	}

	public void onPlayerTurnMessage(NetworkMessage msg){
		GameObject.FindObjectOfType<PlayerSquareMover> ().movePlayer();
	}

	public void onCreatePlayerFromClientMessage(NetworkMessage msg){
		MSGTYPE.CreatePlayerMessage cm = msg.ReadMessage<MSGTYPE.CreatePlayerMessage> ();
		GameObject.FindObjectOfType<GameSetup> ().addPlayerPrefab(cm.player, msg.conn);

	}

	public void onPlayerInitialSelectionFromClientMessage(NetworkMessage msg){
		MSGTYPE.PlayerInitialSelectionMessage pm = msg.ReadMessage<MSGTYPE.PlayerInitialSelectionMessage> ();
		GameObject.FindObjectOfType<PlayerSelection>().SyncListChosenPlayers[pm.whichOne] = true;
		GameObject.FindObjectOfType<PlayerSelection> ().updateList (pm.whichOne, msg.conn.connectionId);

		MSGTYPE.ProvideUniqueIDMessage um = new MSGTYPE.ProvideUniqueIDMessage();
		um.connID = msg.conn.connectionId;
		NetworkServer.SendToClient(msg.conn.connectionId, MSGTYPE.PROVIDEUNIQUEID, um);
	}

	public void onProvideUniqueIDMessage(NetworkMessage msg) {
		MSGTYPE.ProvideUniqueIDMessage um = msg.ReadMessage<MSGTYPE.ProvideUniqueIDMessage> ();
		GameObject.FindObjectOfType<PlayerSelection> ().uniqueConnectionID = um.connID;
	}

	public void onPlayerMovesMessage(NetworkMessage msg) {
		MSGTYPE.PlayerMovesMessage pm = msg.ReadMessage<MSGTYPE.PlayerMovesMessage> ();
		GameObject.FindObjectOfType<PlayerSquareMover> ().movePlayer();
	}

	public void onCardChoiceMessage(NetworkMessage msg) {
		MSGTYPE.CardChoiceMessage cm = msg.ReadMessage<MSGTYPE.CardChoiceMessage> ();
		GameObject.FindObjectOfType<GameSetup> ().showCardSelection(cm.choice);
	}

	public void onGameOverMessage(NetworkMessage msg) {


		MSGTYPE.GameOverMessage go = msg.ReadMessage<MSGTYPE.GameOverMessage> ();
		whoWon = go.whoWon;

		Application.LoadLevel ("GameOverScene");

	}

	void OnLevelWasLoaded (int level){
		if (Application.loadedLevelName == "GameOverScene") {
			GameObject.Find ("GameOver").
				GetComponent<GameOverGUI> ().setWinnerIcon (whoWon);
		}
	}
}
