﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

public class PlayerSelection : NetworkBehaviour {

	private MyNetworkManager nm;
	private Button p1_btn;
	private Button p2_btn;
	private Button p3_btn;

	private Button startgame_btn;

	public int uniqueConnectionID = -1; //each client will have there own value sent by a message from server

	public SyncListBool SyncListChosenPlayers = new SyncListBool();

	//this list will hold unique connection ids - slot1/player1, slot2/player2
	public struct PlayerInfo{
		public int playerID;
		public int connectionID;

		public PlayerInfo(int playerID, int connectionID) {
			this.playerID = playerID;
			this.connectionID = connectionID;
		}
	}

	public class SyncListPlayerInfo : SyncListStruct<PlayerInfo> {}

	public SyncListPlayerInfo SyncList_playerInfo = new SyncListPlayerInfo();

	void Start() {

		DontDestroyOnLoad (this.gameObject);

		nm = GameObject.FindObjectOfType<MyNetworkManager> ();

		p1_btn = GameObject.Find ("Player1").GetComponent<Button> ();
		p2_btn = GameObject.Find ("Player2").GetComponent<Button> ();
		p3_btn = GameObject.Find ("Player3").GetComponent<Button> ();
	
		startgame_btn = GameObject.Find ("StartGame").GetComponent<Button> ();
		startgame_btn.gameObject.SetActive(false);

		p1_btn.onClick.AddListener (delegate () { 
			(this.GetComponent<PlayerSelection> ()).setPlayerInactive (0); 
		});
		p2_btn.onClick.AddListener (delegate () { 
			(this.GetComponent<PlayerSelection> ()).setPlayerInactive (1); 
		});
		p3_btn.onClick.AddListener (delegate () { 
			(this.GetComponent<PlayerSelection> ()).setPlayerInactive (2); 
		});

		startgame_btn.onClick.AddListener (delegate () { 
			(this.GetComponent<PlayerSelection> ()).startGameScene(); 
		});

		if (NetworkServer.active) {
			for (int i=0; i<3; i++) {
				SyncListChosenPlayers.Add (false);
			}
		}
		SyncListChosenPlayers.Callback = this.onChosenPlayersChanged;
		SyncList_playerInfo.Callback = this.onPlayerInfoChanged;

		updateButtons ();
	}
	
	private void onChosenPlayersChanged (SyncList<bool>.Operation op, int index){
		switch (index) {
		case 0:
			p1_btn.interactable = false;
			break;
		case 1:
			p2_btn.interactable = false;
			break;
		case 2:
			p3_btn.interactable = false;
			break;		
		}
	}


	private void onPlayerInfoChanged(SyncListPlayerInfo.Operation op, int index){
		if (checkForAllTrue () && NetworkServer.active)
			startgame_btn.gameObject.SetActive (true);
	}

	public void setPlayerInactive(int whichOne) {
		removeAllListenersOnPlayerSelectButtons ();

		MSGTYPE.PlayerInitialSelectionMessage pm = new MSGTYPE.PlayerInitialSelectionMessage();
		pm.whichOne = whichOne;
		NetworkClient.allClients[0].Send (MSGTYPE.PLAYERINITIALSELECTION, pm);
	}

	public void updateButtons() {
		if (SyncListChosenPlayers [0])
			p1_btn.interactable = false;
		if (SyncListChosenPlayers [1])
			p2_btn.interactable = false;
		if (SyncListChosenPlayers [2])
			p3_btn.interactable = false;
	}

	[Server]
	public void updateList(int playerID, int connectionID) {
		SyncList_playerInfo.Add (new PlayerInfo(playerID,connectionID));
		Debug.Log ("player id = " + playerID + " connectionID = " + connectionID);
	}

	private void startGameScene() {
		nm.GetComponent<MyNetworkManager> ().ServerChangeScene ("BoardGameScene");
	}

	private bool checkForAllTrue() {
		if(SyncList_playerInfo.Count==3) return true;
		else return false;
	}

	private void removeAllListenersOnPlayerSelectButtons (){
		p1_btn.onClick.RemoveAllListeners ();
		p2_btn.onClick.RemoveAllListeners ();
		p3_btn.onClick.RemoveAllListeners ();
	}
}