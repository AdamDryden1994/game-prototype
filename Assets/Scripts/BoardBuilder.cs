﻿using UnityEngine;
using System.Collections;

public class BoardBuilder : MonoBehaviour
{

	public int rows = 20;
	public int columns = 10;

	public GameObject square;


	// Use this for initialization
	void Start ()
	{
		makeBoard (rows, columns);
	}

	private void makeBoard (int rows, int columns)
	{
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < columns; col++) {
				addSquare (row, col);
			}
		}      

	}

	private void addSquare (int row, int col)
	{
		GameObject obj = Instantiate (square, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		Bounds bnd = obj.GetComponentInChildren<Renderer> ().bounds;
		obj.transform.position = new Vector3 (row * bnd.size.x + (bnd.size.x / 2), 1, col * bnd.size.z + (bnd.size.z / 2));

		Vector3 terrainPosition = obj.transform.position;
		terrainPosition.y = Terrain.activeTerrain.SampleHeight (terrainPosition);
		obj.transform.position = terrainPosition + new Vector3 (0, 1f, 0);

		if (obj.transform.position.y > 1.5f)
			Destroy (obj);
	}
}
