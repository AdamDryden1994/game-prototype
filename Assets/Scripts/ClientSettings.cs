﻿using UnityEngine;
using System.Collections;

/*
 * This script will hold individual settings for each client (Note that the host
 * is a client is this regard). Values will be set for the character selection that
 * a client will make on the player select screen. This value will be used to later
 * instantiate the slected player once a network connection has been made
 * 
 * Each player can select a map - the final choice of map will be determined by the
 * map that is the modt popular. As the game is 4 players - a 2/2  or a 1/1/1/1 tie will
 * result in the 1st map being chosen
 * 
 * In the test version only 1 map is available and this feature will not be implemented
 * although the option to choose a map and retain this value will be avaiilable
 * 
 */



public class ClientSettings  {

	public const int AMERICAN = 0;
	public const int BRITISH = 1;
	public const int FRENCH = 2;
	public const int RUSSIAN = 3;

	public static int characterSelection = -1;
	public static int mapSelection = -1;


}
