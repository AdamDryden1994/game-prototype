﻿using UnityEngine;
using System.Collections;

public class MusicVolume : MonoBehaviour {

	// Use this for initialization
	void Start()
	{
		if (AudioListener.volume == 1)
		{
			AudioListener.volume = 0;
		}
	}
}
