﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverGUI : MonoBehaviour {

	public Sprite player1Sprite;
	public Sprite player2Sprite;
	public Sprite player3Sprite;
	
	public Image winnerIcon;

	private PlayerSelection.PlayerInfo[] playerInfoList;

	void Awake () {

		playerInfoList = new PlayerSelection.PlayerInfo[3];
		GameObject.Find("PlayerSelection").
			GetComponent<PlayerSelection>().SyncList_playerInfo.CopyTo(playerInfoList,0);

	}

	public void setWinnerIcon(int whoWon) {
		PlayerSelection.PlayerInfo pi = playerInfoList[whoWon];
		changeImageSprite(pi.playerID);
	}

	private void changeImageSprite(int playerID) {
		Debug.Log ("PlayerID - " + playerID);
		switch(playerID) {
		case 0:
			winnerIcon.sprite = player1Sprite;
			break;
		case 1:
			winnerIcon.sprite = player2Sprite;
			break;
		case 2:
			winnerIcon.sprite = player3Sprite;
			break;
		}
	}

}
