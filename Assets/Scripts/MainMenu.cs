using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public string startLevel;

    public string levelSelect;

	/* The version of this accesed on the Character Selection page has been moved to the
	 * Character Selection Script as the value for which character
	 * is displayed os set there
	 */
    public void NewGame()
    {
        Application.LoadLevel(startLevel);
    }
    

	public void LevelSelect(int map)
    {
		ClientSettings.mapSelection = map;
        Application.LoadLevel("Menu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }




}
