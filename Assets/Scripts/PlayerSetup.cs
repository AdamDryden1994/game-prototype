﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerSetup : NetworkBehaviour {

	public int playerID = -1; //will hold 0, 1, 2 depending on the player selected in selection page
	public NetworkConnection conn; //only held on server currently
	private PlayerSelection.PlayerInfo[] playerInfoList;

	// Use this for initialization
	void Start () {
		Debug.Log ("start");
		DontDestroyOnLoad (this.gameObject);
		playerID = ClientSettings.characterSelection;

		if (this.GetComponent<NetworkIdentity> ().isLocalPlayer) {
			this.gameObject.AddComponent<PlayerSquareMover> ();
			//this.gameObject.GetComponent<PlayerMovement> ().enabled = true;
			//Camera.main.transform.SetParent (this.transform);
			switch (playerID) {

			case 0:
				Camera.main.transform.position = GameObject.Find ("P0CAM").transform.position;
				Camera.main.transform.rotation = GameObject.Find ("P0CAM").transform.rotation;
				break;
			case 1:
				Camera.main.transform.position = GameObject.Find ("P1CAM").transform.position;
				Camera.main.transform.rotation = GameObject.Find ("P1CAM").transform.rotation;
				break;
			case 2:
				Camera.main.transform.position = GameObject.Find ("P2CAM").transform.position;
				Camera.main.transform.rotation = GameObject.Find ("P2CAM").transform.rotation;
				break;
			case 3:
				Camera.main.transform.position = GameObject.Find ("P3CAM").transform.position;
				Camera.main.transform.rotation = GameObject.Find ("P3CAM").transform.rotation;
				break;
				

			}
			Camera.main.transform.LookAt(this.transform);
			//GameObject.Find ("PlayerIcon").GetComponent<Image> ().color = new Color (1, 1, 1, 0.3f);
		} else {
			Destroy(this.gameObject.GetComponent<PlayerSetup> ());
		}


	

	}

	void  OnStartLocalPlayer() {
		Debug.Log ("start local player");
	}

	void Update() {
		if (Input.GetKey (KeyCode.Y)) {
			Camera.main.transform.Translate (Vector3.forward, Space.World);
		}
		if (Input.GetKey (KeyCode.B)) {
			Camera.main.transform.Translate (-Vector3.forward, Space.World);
		}
		if (Input.GetKey (KeyCode.G)) {
			Camera.main.transform.Translate (Vector3.left, Space.World);
		}
		if (Input.GetKey (KeyCode.H)) {
			Camera.main.transform.Translate (Vector3.right, Space.World);
		}

	}


}
